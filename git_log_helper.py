import sys,getopt
import re

FROM_NEWEST = 0
FROM_OLDEST = 1

order = FROM_NEWEST

opts,args= getopt.getopt(sys.argv[1:], "s:of:")
for op,value in opts:
	if op=="-f":
		filename = value
	elif op=="-s":
		search_str = value
	elif op=="-o":
		order = FROM_OLDEST	

def parse_commit(commit, search_str):
	if temp_commit == "":
		return

	r = re.search(search_str, temp_commit)
	if r != None:
		commits.append(temp_commit)

commits = []
temp_commit = ""
file = open(filename,'r')
for line in file:
	r = re.search("commit", line)
	if r != None:
		parse_commit(temp_commit, search_str)
		temp_commit = line
	else:
		temp_commit += line

print str(len(commits))+" commits found."

i = 0
while(i<len(commits)):
	raw_input("Press any key to see the "+str(i+1)+"th commit")
	print "------------------"+str(i+1)+"----------------------"
	if order==FROM_OLDEST:
		print commits[-i-1]
	else:
		print commits[i]
	i += 1
